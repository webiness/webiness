Webiness, a lightweight PHP framework
=====================================

## Description

Webiness is PHP framework with MVC design pattern. It 
has a very straightforward installation process that requires only a 
minimal configuration, so it can save you a lot of hassle. Also, it 
canbe an ideal choice if you want to avoid PHP version conflict, as it 
works on all PHP versions from 5.3. Webiness extensively uses the lazy 
loading technique so, in most cases, it is faster then other frameworks. 
It can be used for rapid prototyping of web applications. Webiness is 
integrated with jQuery, and it comes with a set of AJAX-enabled features 
and his own light CSS framework for frontend development. Trying to be 
secure, it has integrated authetification and authorization module, RBAC 
user interface, sessions expire feature, input validation, CSRF 
protection, SQL injection prevention and other security features.



## Features

* Model View Controller - MVC
* Data Access Object - DAO
* Active Record - AR
* Authentication & Authorization
* Role Based Access Control - RBAC
* Representational State Transfer - REST
* CSRF Tokens, Session Expiration, SQL Injection
* PostgreSQL, MySQL, MariaDB
* HTML5, AJAX, JQuery, CSS3,
* CRUD Grid, Chart, Image manipulation
* Rapid Application Development - RAD
* Easy coding
* Lazy loading



## Installation

1. download copy of webiness framework
2. extract it in your webserver document root



## Requirements

* PHP version 5.3 or above
* Optional: PHP PDO module
* Optional: PHP GD module



## License

The project is distributed under [MIT LICENSE](https://opensource.org/licenses/MIT)
