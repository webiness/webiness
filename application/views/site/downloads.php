<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/highlight.min.js"></script>

<script>hljs.initHighlightingOnLoad();</script><div class="row">
    <div class="column column-8 column-offset-2">
        <h1 class="text-primary">Download and Installation</h1>
        Webiness is an open source project released under the terms of the
        MIT License. This means that you can use Webiness for free to develop either
        open-source or proprietary Web applications.
        <br/>
        <br/>
        <h3 class="text-primary">Install from archive file</h3>
        Download latest version from sourceforge.net, and then extract it to a
        Web-accessible folder:
        <ul>
            <li>
                <a
                href="https://sourceforge.net/projects/webiness/files/latest/download?source=directory">
                    webiness-1.7.2.tar.xz
                </a>
            </li>
        </ul>
        <h3 class="text-primary">Obtain the Latest Code</h3>
        <pre>
            <code class="git">
                git clone https://webiness.github.com/webiness
            </code>
        </pre>
        Webiness is hosted on
        <a href="https://github.com/webiness/webiness">GitHub</a>.
        You are welcome to contribute to the Webiness development by submitting
        <a href="https://github.com/webiness/webiness/issues">issue
        reports</a> or
        <a href="https://github.com/webiness/webiness/pulls">merge requests</a>.
    </div>
</div>
