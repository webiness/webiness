<br/>
<br/>
<div class="row" >
    <div class="column column-8 column-offset-2">
	<h3 class="text-success">
	    Welcome to - <?php echo WsConfig::get('app_name'); ?> -
	</h3>
	Congratulations. You have successfuly created your Webiness-powered application.
	
	<br/>
	<br/>
	<br/>
	You may change the content of this page by modifying the following two files:
	<ul>
	    <li>View file:
		<span class="text-error">
		    <?php echo WsROOT.'/application/view/site/index.php'; ?>
		</span>
	    </li>
	    <li>Layout file:
		<span class="text-error">
		    <?php echo WsROOT.'/public/layouts/webiness.php'; ?>
		</span>
	    </li>
	</ul>
	
	<br/>
	<br/>
	For more details on how to further develop this application, please read the
	<a target="_blank" href="http://webiness.sourceforge.net/index.php?request=site/intro/">introduction</a>
	and <a target="_blank" href="http://webiness.sourceforge.net/doc/index.html">class reference</a>
	or follow our <a target="_blank" href="http://webiness.sourceforge.net/index.php?request=site/guide/">
	tutorial</a>.
    </div>
</div>
<br/>
<br/>
